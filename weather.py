"""
This script contains the Weather class.

:Author: BJ Peter Dela Cruz
"""
from datetime import datetime
from math import trunc
import pytz

# pylint: disable=too-many-instance-attributes
# pylint: disable=too-few-public-methods


class Weather:
    """
    This class contains weather information for a geographic location.
    """
    def __init__(self, json, city):
        """
        Populates this object with weather information such as temperature, wind speed, cloud
        coverage, etc.

        :param json: The JSON string containing weather information
        :param city: The name of the city
        """
        self.name = json["name"]
        self.latitude = json["coord"]["lat"]
        self.longitude = json["coord"]["lon"]
        self.main = []
        self.description = []
        for weather in json["weather"]:
            self.main.append(weather["main"])
            self.description.append(weather["description"])
        self.temperature = f"{json['main']['temp']}\xb0 F"
        self.feels_like = f"{json['main']['feels_like']}\xb0 F"
        self.wind_speed = f"{json['wind']['speed']} MPH"
        self.wind_degrees = f"{json['wind']['deg']}\xb0"
        self.clouds = f"{json['clouds']['all']}%"
        self.data_calculated_on = convert_unix_timestamp(int(json["timezone"]), int(json["dt"]))
        self.sunrise = convert_unix_timestamp(int(json["timezone"]), int(json["sys"]["sunrise"]))
        self.sunset = convert_unix_timestamp(int(json["timezone"]), int(json["sys"]["sunset"]))
        self.city = city
        self.coordinates = f"{convert_to_decimal_minutes(self.latitude, True)}, " \
                           f"{convert_to_decimal_minutes(self.longitude, False)}"


def convert_unix_timestamp(timezone: int, timestamp: int) -> str:
    """
    This function converts a UNIX timestamp to a 12-hour clock format with AM/PM.

    :param timezone: The timezone
    :param timestamp: The timestamp
    :return: A 12-hour clock format with AM/PM
    """
    return datetime.fromtimestamp(timestamp + timezone, tz=pytz.utc).strftime('%I:%M %p')


def convert_to_decimal_minutes(coordinate: float, is_latitude: bool):
    """
    This function converts a latitude or longitude coordinate that is in decimal degrees to a
    coordinate that is in decimal minutes.

    :param coordinate: The latitude or longitude coordinate
    :param is_latitude: True if the given coordinate represents latitude, False if longitude
    :return: A coordinate that is in decimal minutes format
    """
    degrees = trunc(coordinate)
    minutes = trunc((coordinate - degrees) * 60)
    if is_latitude:
        direction = 'N' if coordinate > 0 else 'S'
    else:
        direction = 'E' if coordinate > 0 else 'W'
    if coordinate < 0:
        degrees *= -1
        minutes *= -1
    return f"{degrees}\xb0 {minutes}' {direction}"

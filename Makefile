DOCKER=docker

all: build_and_run

stop_and_rm:
	if $(DOCKER) ps -a | grep -q weather-now-app; \
	then $(DOCKER) stop weather-now-app && $(DOCKER) rm weather-now-app; \
	fi

build_and_run: stop_and_rm
	$(DOCKER) build -t weather-now . && \
	  $(DOCKER) run -dit --name weather-now-app -p 80:8080 -e MODULE_NAME=app \
	    -e VARIABLE_NAME=app \
	      -e OPENWEATHER_API_KEY=${OPENWEATHER_API_KEY} \
	        -e OPENAI_API_KEY=${OPENAI_API_KEY} \
	          -e AZURE_CLIENT_ID=${WEATHER_NOW_CLIENT_ID} \
	            -e AZURE_TENANT_ID=${ARM_TENANT_ID} \
	              -e AZURE_CLIENT_SECRET=${WEATHER_NOW_CLIENT_SECRET} weather-now

build:
	$(DOCKER) build -t weather-now .

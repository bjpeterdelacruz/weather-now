terraform {
  backend "s3" {
    bucket = "dev-bjdelacruz-terraform"
    key    = "weather-now/state.tfstate"
    region = "us-east-2"
  }
}

provider "google" {
  project = var.GOOGLE_CLOUD_PROJECT_ID
  region  = "us-west1"
}

variable "GOOGLE_CLOUD_PROJECT_ID" {
  type = string
}

variable "AZURE_TENANT_ID" {
  type = string
}

variable "OPENAI_API_KEY" {
  type      = string
  sensitive = true
}

variable "OPENWEATHER_API_KEY" {
  type      = string
  sensitive = true
}

variable "WEATHER_NOW_CLIENT_ID" {
  type = string
}

variable "WEATHER_NOW_CLIENT_SECRET" {
  type      = string
  sensitive = true
}

resource "google_cloud_run_service" "weather_now" {
  name     = "weather-now"
  location = "us-west1"

  template {
    spec {
      containers {
        image = "bjdelacruz/weather-now:latest"

        resources {
          limits = {
            memory = "256Mi"
            cpu    = "1"
          }
        }

        env {
          name  = "AZURE_CLIENT_ID"
          value = var.WEATHER_NOW_CLIENT_ID
        }

        env {
          name  = "AZURE_CLIENT_SECRET"
          value = var.WEATHER_NOW_CLIENT_SECRET
        }

        env {
          name  = "AZURE_TENANT_ID"
          value = var.AZURE_TENANT_ID
        }

        env {
          name  = "MODULE_NAME"
          value = "app"
        }

        env {
          name  = "OPENWEATHER_API_KEY"
          value = var.OPENWEATHER_API_KEY
        }

        env {
          name  = "OPENAI_API_KEY"
          value = var.OPENAI_API_KEY
        }

        env {
          name  = "VARIABLE_NAME"
          value = "app"
        }
      }
    }
  }

  traffic {
    percent         = 100
    latest_revision = true
  }
}

resource "google_cloud_run_service_iam_policy" "public_access" {
  location = google_cloud_run_service.weather_now.location
  service  = google_cloud_run_service.weather_now.name

  policy_data = <<EOF
{
  "bindings": [
    {
      "role": "roles/run.invoker",
      "members": [
        "allUsers"
      ]
    }
  ]
}
EOF
}

output "cloud_run_service_url" {
  description = "The URL of the deployed Cloud Run service"
  value       = google_cloud_run_service.weather_now.status[0].url
}

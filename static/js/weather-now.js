'use strict';

document.addEventListener("DOMContentLoaded", function() {
    if (!navigator.geolocation) {
        console.log("Geolocation is not supported by this browser.");
        document.getElementById("geoBtn").disabled = true;
    }
    document.getElementById("year").innerHTML = new Date().getFullYear().toString();
    disableButtons();

    document.getElementById("api_key_input").addEventListener('input', updateBtnState);
    document.getElementById("city_name_input").addEventListener('input', updateCityBtnState);
    document.getElementById("city_name_input").addEventListener('propertychange', updateCityBtnState);

    document.getElementById("latitude_input").addEventListener('input', updateLatLonBtnState);
    document.getElementById("latitude_input").addEventListener('propertychange', updateLatLonBtnState);
    document.getElementById("longitude_input").addEventListener('input', updateLatLonBtnState);
    document.getElementById("longitude_input").addEventListener('propertychange', updateLatLonBtnState);
});

const updateBtnState = function() {
    updateCityBtnState();
    updateLatLonBtnState();
    document.getElementById("geoBtn").disabled = !document.getElementById("api_key_input").value;
}

const updateCityBtnState = function() {
    document.getElementById("cityBtn").disabled =
        !(document.getElementById("api_key_input").value && document.getElementById("city_name_input").value);
}

const updateLatLonBtnState = function() {
    document.getElementById("latLonBtn").disabled =
        !(document.getElementById("api_key_input").value && document.getElementById("latitude_input").value
            && document.getElementById("longitude_input").value);
}

function useGeolocation() {
    navigator.geolocation.getCurrentPosition(getCurrentWeatherPosition, getCurrentWeatherDefault);
}

async function getCurrentWeatherDefault() {
    await updateDisplayCity("Puyallup");
}

async function getCurrentWeatherPosition(position) {
    const lat = position.coords.latitude;
    const lon = position.coords.longitude;
    document.getElementById("message").innerHTML =
        `<div><h2>Fetching weather for current geolocation. Please wait... ⏳</h2></div>`;
    document.getElementById("message").removeAttribute("style");
    document.getElementById("generated_content").setAttribute("style", "display: none");
    document.getElementById("error").setAttribute("style", "display: none");
    await updateDisplayLatLon(lat, lon);
}

async function updateDisplayLatLon(lat, lon) {
    document.getElementById("error").setAttribute("style", "display: none");
    disableButtons();
    const apiKey = document.getElementById("api_key_input").value;
    const response = await fetch(`/api/weather/${apiKey}/${lat}/${lon}`);
    if (!response.ok) {
        await displayError(response);
        return;
    }
    document.getElementById("message").setAttribute("style", "display: none");
    document.getElementById("generated_content").innerHTML = await response.text();
    document.getElementById("generated_content").removeAttribute("style");
    updateBtnState();
}

function getCurrentWeatherCity() {
    updateDisplayCity(document.getElementById("city_name_input").value);
}

function disableButtons() {
    document.getElementById("cityBtn").disabled = true;
    document.getElementById("latLonBtn").disabled = true;
    document.getElementById("geoBtn").disabled = true;
}

async function displayError(response) {
    const error = await response.text();
    document.getElementById("error").innerHTML = `<div><h2>${error}</h2></div>`;
    document.getElementById("error").removeAttribute("style");
    document.getElementById("generated_content").setAttribute("style", "display: none");
    document.getElementById("message").setAttribute("style", "display: none");
    updateBtnState();
}

async function updateDisplayCity(city) {
    document.getElementById("error").setAttribute("style", "display: none");
    document.getElementById("generated_content").removeAttribute("style");
    document.getElementById("generated_content").innerHTML =
        `<div><h2>Fetching weather for ${city}. Please wait... ⏳</h2></div>`;
    disableButtons();
    const apiKey = document.getElementById("api_key_input").value;
    const response = await fetch(`/api/weather/${apiKey}/${city}`);
    if (!response.ok) {
        await displayError(response);
        return;
    }
    document.getElementById("message").setAttribute("style", "display: none");
    document.getElementById("generated_content").innerHTML = await response.text();
    updateBtnState();
}

function isNumeric(input) {
    return /^-?\d*\.?\d+$/.test(input);
}

async function getCurrentWeatherLatLon() {
    const lat = document.getElementById("latitude_input").value;
    const lon = document.getElementById("longitude_input").value;

    if (!isNumeric(lat) || parseFloat(lat) > 90 || parseFloat(lat) < -90) {
        alert("Please enter a valid number between -90 and 90 for latitude.");
        return;
    }
    if (!isNumeric(lon) || parseFloat(lon) > 180 || parseFloat(lon) < -180) {
        alert("Please enter a valid number between -180 and 180 for longitude.");
        return;
    }
    document.getElementById("message").innerHTML =
        `<div><h2>Fetching weather for geographic coordinates. Please wait... ⏳</h2></div>`;
    document.getElementById("message").removeAttribute("style");
    document.getElementById("generated_content").setAttribute("style", "display: none");
    document.getElementById("error").setAttribute("style", "display: none");

    await updateDisplayLatLon(lat, lon);
}

# Weather Now!

This Flask web application uses the OpenWeather API to fetch the latest weather data for a city or
geographic location. It can also fetch weather data for a user's current location using a web
browser's geolocation API. The weather data is sent to OpenAI, and the `gpt-4o` model will generate
a fun response for the user.

Live demo: [https://weather-now-knartlettq-uw.a.run.app](https://weather-now-knartlettq-uw.a.run.app).

![Saipan](https://bjdelacruz.dev/files/weathernow1.png)

![Unknown Location](https://bjdelacruz.dev/files/weathernow2.png)

![Puyallup](https://bjdelacruz.dev/files/weathernow3.png)

## Developer Notes

### Building and Running the Application

Execute the following commands to create a new environment, install dependencies, and start the web
application in **DEVELOPMENT** mode.

```
python -m venv venv
source venv/bin/activate
pip install -r requirements.txt
python app.py
```

### Building a Docker Image

Add the API keys for [OpenWeather](https://openweathermap.org/api) and [OpenAI](https://openai.com/) as environment variables:

* OPENWEATHER_API_KEY
* OPENAI_API_KEY

Then use the `make` command without specifying a target to build the image and run it in a container:

```
make
```

### Downloading the Latest Docker Image of Weather Now (Optional)

```
docker pull bjdelacruz/weather-now:latest
```

## Profiles

- [Developer's Website](https://bjdelacruz.dev)
- [Coderbyte](https://coderbyte.com/profile/bjpeterdelacruz)
- [Codesignal](https://app.codesignal.com/profile/bjpeter)
- [Codewars](https://www.codewars.com/users/bjpeterdelacruz/stats)

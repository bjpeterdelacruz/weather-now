"""
This script contains all the routes for this web application.

:Author: BJ Peter Dela Cruz
"""
from flask import Flask, render_template
from openai_gptmodel import generate_response
from openweather import get_current_weather, get_current_weather_latlon
from openweather import CityNotFoundError, InvalidCoordinatesError
import json
from azurekeyvault import api_key as api_key_from_kv


app = Flask(__name__)


@app.route('/')
def index():
    """
    This function serves the home page to the user.

    :return: The home page
    """
    return render_template("index.html")


@app.route('/api/weather/<string:api_key>/<string:city_name>')
def current_weather(api_key: str, city_name: str):
    """
    This function gets the current weather information for :attr:`city_name`.

    :param api_key: The API key
    :param city_name: The city name
    :return: Weather information in JSON format
    """
    try:
        if api_key != api_key_from_kv:
            return "API key 🔑 is missing or does not match. 😔", 400
        return generate_response(json.dumps(get_current_weather(city_name).__dict__))
    except CityNotFoundError:
        return f"The city {city_name} 🏙️ was not found. 😔", 400


@app.route('/api/weather/<string:api_key>/<string:lat>/<string:lon>')
def current_weather_latlon(api_key: str, lat: str, lon: str):
    """
    This function gets the current weather information for a latitude and longitude.

    :param api_key: The API key
    :param lat: The latitude
    :param lon: The longitude
    :return: Weather information in JSON format
    """
    try:
        if api_key != api_key_from_kv:
            return "API key 🔑 is missing or does not match. 😔", 400
        return generate_response(
            json.dumps(get_current_weather_latlon(lat, lon, "").__dict__))
    except InvalidCoordinatesError:
        return f"{lat} and {lon} 🌎 are invalid geographic coordinates. 😔", 400

import os
from azure.identity import DefaultAzureCredential
from azure.keyvault.secrets import SecretClient

# Local run
if "AZURE_CLIENT_ID" not in os.environ:
    os.environ["AZURE_CLIENT_ID"] = os.getenv("WEATHER_NOW_CLIENT_ID")
    os.environ["AZURE_TENANT_ID"] = os.getenv("ARM_TENANT_ID")
    os.environ["AZURE_CLIENT_SECRET"] = os.getenv("WEATHER_NOW_CLIENT_SECRET")
    print("Loaded environment variables from local machine.")

key_vault_name = "kv-weather-now"
KV_URI = f"https://{key_vault_name}.vault.azure.net"

credential = DefaultAzureCredential()
client = SecretClient(vault_url=KV_URI, credential=credential)

secret_name = "api-key"

api_key = client.get_secret(secret_name).value
print("Successfully retrieved API key from Key Vault!")

"""
This script contains utility functions.

:Author: BJ Peter Dela Cruz
"""
from os import getenv
import requests

from utilities import lookup_city_country
from weather import Weather


OPENWEATHER_API_URL = "https://api.openweathermap.org/data/2.5/weather?"


class CityNotFoundError(Exception):
    """
    This error is raised if a city cannot be found on Open Weather.
    """


class InvalidCoordinatesError(Exception):
    """
    This error is raised if either the latitude or longitude is invalid.
    """


def get_current_weather_latlon(lat: str, lon: str, city: str) -> Weather:
    """
    This function makes a remote call to the OpenWeather API to retrieve weather information for the
    given latitude and longitude.

    :param lat: The latitude
    :param lon: The longitude
    :param city: The city
    :return: A Weather object containing weather information for the given latitude and longitude
    """
    try:
        latitude = float(lat)
        if latitude > 90 or latitude < -90:
            raise InvalidCoordinatesError("Latitude must be between -90 and 90, inclusive.")
        longitude = float(lon)
        if longitude > 180 or longitude < -180:
            raise InvalidCoordinatesError("Longitude must be between -180 and 180, inclusive.")
    except ValueError as error:
        raise InvalidCoordinatesError("Invalid value for latitude or longitude.") from error
    params = f"lat={lat}&lon={lon}&appid={getenv('OPENWEATHER_API_KEY')}&units=imperial"
    current_weather = requests.get(f"{OPENWEATHER_API_URL}{params}").json()
    return Weather(current_weather, city)


def get_current_weather(city_name: str) -> Weather:
    """
    This function makes a remote call to the OpenWeather API to retrieve weather information for
    :attr:`city_name`.

    :param city_name: The city name
    :return: A Weather object containing weather information for :attr:`city_name`
    """
    city = lookup_city_country(city_name)
    params = f"q={city}&appid={getenv('OPENWEATHER_API_KEY')}&units=imperial"
    current_weather = requests.get(f"{OPENWEATHER_API_URL}{params}").json()
    if current_weather["cod"] == "404":
        raise CityNotFoundError(f"City not found: {city_name}")
    return Weather(current_weather, city_name)

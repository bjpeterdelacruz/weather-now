system_prompt = """
You are a helpful, funny assistant that gives weather information. Only respond to requests for
weather information.
"""

prompt = """
Given this JSON string, please describe the weather in a fun way. The output should be formatted
using HTML tags. Only produce an HTML fragment that can be placed inside an existing HTML document.
Be descriptive and funny when describing the location in the response. Include emojis. The response
should not have ```html preceding the HTML code and ``` following it. Generate a catchy title in
the <h2> tag.

{json}

Do not use any <li> in the HTML code. For all the weather data in the JSON string, please write at
least one funny sentence to brighten someone's day, but make sure that the date when the weather
data was calculated comes last. In the response, always include the sunrise and sunset times, and
use complete sentences. Finally, write one fun fact about the location, up to a maximum of three
sentences. Mention the state or country in which it is located. The fun fact always comes last in
the response.
"""
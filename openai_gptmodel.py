import logging
from langchain_community.chat_models import ChatOpenAI
from prompts import system_prompt, prompt


def generate_response(json: str):
    logging.info(f"JSON response: {json}")

    messages = [("system", system_prompt), ("user", prompt.format(json=json))]

    return ChatOpenAI(model_name='gpt-4o').invoke(messages).content


if __name__ == '__main__':
    print(generate_response("""
    {
        "city": "Saipan",
        "clouds": "75%",
        "coordinates": "15\u00b0 8' N, 145\u00b0 42' E",
        "description": [
            "broken clouds"
        ],
        "feels_like": "92.55\u00b0 F",
        "latitude": 15.1355,
        "longitude": 145.701,
        "main": [
            "Clouds"
        ],
        "name": "Saipan",
        "sunrise": "05:54 AM",
        "sunset": "06:51 PM",
        "temperature": "83.1\u00b0 F",
        "wind_degrees": "90\u00b0",
        "wind_speed": "12.66 MPH"
    }
    """))
